//
//  fastReaderViewController.h
//  FastReader
//
//  Created by Roman Sladecek on 11/20/12.
//  Copyright (c) 2012 Roman Sladecek. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MAIN_VIEW 1
#define READING_VIEW 2
#define SETTINGS_VIEW 3

@interface fastReaderViewController : UIViewController <UITextFieldDelegate> {
    
    int countOfWords; // how many words are in textView
    int count;
    
    int howManyWords;
    int speed;
    
    UIButton *btn_start;// button for start/stop reading
    BOOL isReading;     // is or isn't reading now
    int choosenBook;
    UILabel *lbl_bookTitle;
    UILabel *lbl_words;
    UILabel *lbl_speed;
    
    int actualView;
    
    UIView *mainView;
    UIView *readingView;
    UIView *settingsView;
    
    UIToolbar *toolbar; // on main screen
    UIBarButtonItem *settingslButton;
    
    UIButton *btn_book1;
    UIButton *btn_book2;
    UIButton *btn_book3;
    UIButton *btn_book4;
    UIButton *btn_book5;

    UIToolbar *toolbar_settings; // toobar on settings view
    UILabel *lbl_settings_wordCount;
    UIStepper* stepper;
    UILabel *lbl_settings_speed;
    UISlider *slider;
    float sliderStepValue;
    
}

@property (nonatomic, retain) UILabel *lbl;
@property (nonatomic, retain) UITextField *textField;
@property (nonatomic, retain) UITextView *textView;
@property (nonatomic, retain) NSTimer *repeatingTimer;



@end
