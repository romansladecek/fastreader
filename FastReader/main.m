//
//  main.m
//  FastReader
//
//  Created by Roman Sladecek on 11/20/12.
//  Copyright (c) 2012 Roman Sladecek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "fastReaderAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([fastReaderAppDelegate class]));
    }
}
