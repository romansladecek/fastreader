//
//  fastReaderViewController.m
//  FastReader
//
//  Created by Roman Sladecek on 11/20/12.
//  Copyright (c) 2012 Roman Sladecek. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "fastReaderViewController.h"

@interface fastReaderViewController ()

@end

@implementation fastReaderViewController

@synthesize lbl, textField, textView, repeatingTimer;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    howManyWords = 1;
    speed = 60;
    count = 0;
    isReading = NO;
    actualView = MAIN_VIEW;
    choosenBook = NO;

    
    // Main View
    mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    mainView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [self.view addSubview:mainView];
    
    // Reading View
    readingView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    readingView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background_reading.png"]];
    [self.view addSubview:readingView];
    
    // Modal View where picker is
    settingsView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 200)];
    [settingsView setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    [self.view addSubview:settingsView];
        
    [self initMainView];
    [self initSettingsView];
    [self initReadingView];

    // Toolbar on bottom
    toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, self.view.frame.size.height-44, self.view.frame.size.width, 44);
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [toolbar setItems:items animated:NO];
    [self.view addSubview:toolbar];
    settingslButton = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonSystemItemEdit target:self action:@selector(btn_showSettings)];
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = [NSArray arrayWithObjects: settingslButton, flexible, nil];

}

-(void)initMainView {
    
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 118)];
    [logoView setImage:[UIImage imageNamed:@"logo.png"]];
    [mainView addSubview:logoView];
    
    UIImageView *arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 280, 320, 108)];
    [arrowView setImage:[UIImage imageNamed:@"arrowText.png"]];
    [mainView addSubview:arrowView];
    
    btn_book1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];//[UIButton buttonWithType:UIButtonTypeCustom];
    btn_book1.layer.cornerRadius = 5;
    btn_book1.layer.masksToBounds = YES;
    [btn_book1 addTarget:self
                  action:@selector(loadBook:)
        forControlEvents:UIControlEventTouchUpInside];
    [btn_book1 setBackgroundImage:[UIImage imageNamed:@"book1.png"]
                         forState:UIControlStateNormal];
    btn_book1.frame = CGRectMake(12, 135, 92, 98);
    btn_book1.tag = 1;
    [mainView addSubview:btn_book1];

    btn_book2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];//[UIButton buttonWithType:UIButtonTypeCustom];
    btn_book2.layer.cornerRadius = 5;
    btn_book2.layer.masksToBounds = YES;
    [btn_book2 addTarget:self
                  action:@selector(loadBook:)
        forControlEvents:UIControlEventTouchUpInside];
    [btn_book2 setBackgroundImage:[UIImage imageNamed:@"book2.png"]
                         forState:UIControlStateNormal];
    btn_book2.frame = CGRectMake(112, 135, 92, 98);
    btn_book2.tag = 2;
    [mainView addSubview:btn_book2];
    
    btn_book3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];//[UIButton buttonWithType:UIButtonTypeCustom];
    btn_book3.layer.cornerRadius = 5;
    btn_book3.layer.masksToBounds = YES;
    [btn_book3 addTarget:self
                  action:@selector(loadBook:)
        forControlEvents:UIControlEventTouchUpInside];
    [btn_book3 setBackgroundImage:[UIImage imageNamed:@"book3.png"]
                         forState:UIControlStateNormal];
    btn_book3.frame = CGRectMake(212, 135, 92, 98);
    btn_book3.tag = 3;
    [mainView addSubview:btn_book3];
    
    btn_book4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];//[UIButton buttonWithType:UIButtonTypeCustom];
    btn_book4.layer.cornerRadius = 5;
    btn_book4.layer.masksToBounds = YES;
    [btn_book4 addTarget:self
                  action:@selector(loadBook:)
        forControlEvents:UIControlEventTouchUpInside];
    [btn_book4 setBackgroundImage:[UIImage imageNamed:@"book4.png"]
                         forState:UIControlStateNormal];
    btn_book4.frame = CGRectMake(12, 250, 92, 98);
    btn_book4.tag = 4;
    [mainView addSubview:btn_book4];
    
    btn_book5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];//[UIButton buttonWithType:UIButtonTypeCustom];
    btn_book5.layer.cornerRadius = 5;
    btn_book5.layer.masksToBounds = YES;
    [btn_book5 addTarget:self
                  action:@selector(loadBook:)
        forControlEvents:UIControlEventTouchUpInside];
    [btn_book5 setBackgroundImage:[UIImage imageNamed:@"book5.png"]
                         forState:UIControlStateNormal];
    btn_book5.frame = CGRectMake(112, 250, 92, 98);
    btn_book5.tag = 5;
    [mainView addSubview:btn_book5];
    
}

-(void)initReadingView {
    
    lbl_bookTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,18,300,30)];
    [lbl_bookTitle setTextAlignment:NSTextAlignmentCenter];
    lbl_bookTitle.textColor = [UIColor blackColor];
    lbl_bookTitle.backgroundColor = [UIColor clearColor];
    lbl_bookTitle.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(14.0)];
    [readingView addSubview:lbl_bookTitle];
    lbl_bookTitle.text = @"Title of Book";

    // source text
    CGRect textViewFrame = CGRectMake(45.0f, 70.0f, 230.0f, 128.0f);
    textView = [[UITextView alloc] initWithFrame:textViewFrame];
    textView.text = @"Jedna dva tri styri pat sest sedem osem devat desat";
    textView.backgroundColor = [UIColor clearColor];
    [textView setEditable:NO];
    [readingView addSubview:textView];
    
    
    lbl_words = [[UILabel alloc] initWithFrame:CGRectMake(100,207,20,30)];
    [lbl_words setTextAlignment:NSTextAlignmentCenter];
    lbl_words.textColor = [UIColor whiteColor];
    lbl_words.backgroundColor = [UIColor clearColor];
    lbl_words.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16.0)];
    [readingView addSubview:lbl_words];
    lbl_words.text = @"3";
    
    lbl_speed = [[UILabel alloc] initWithFrame:CGRectMake(195,207,80,30)];
    [lbl_speed setTextAlignment:NSTextAlignmentLeft];
    lbl_speed.textColor = [UIColor whiteColor];
    lbl_speed.backgroundColor = [UIColor clearColor];
    lbl_speed.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16.0)];
    [readingView addSubview:lbl_speed];
    lbl_speed.text = @"1000 ppm";
    
    
    // reading label
    lbl = [[UILabel alloc] initWithFrame:CGRectMake(30,268,260,50)];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    lbl.textColor = [UIColor blackColor];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16.0)];
    [readingView addSubview:lbl];
    lbl.text = @"";
    
    
    // Button - Start reading/Stop Reading
    btn_start= [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_start setFrame:CGRectMake(40, 350, 239, 61)];
    UIImage *btn_read = [UIImage imageNamed:@"btn_green.png"];
    [btn_start setBackgroundImage:btn_read
                            forState:UIControlStateNormal];
    [btn_start setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_start  addTarget:self action:@selector(btn_startReading) forControlEvents:UIControlEventTouchUpInside];
    [btn_start setTitle:@"Start reading" forState:UIControlStateNormal];
    [readingView addSubview:btn_start];
    
}

// init all Settings elements
-(void)initSettingsView {
    
    // Toolbar above picker
    toolbar_settings = [[UIToolbar alloc] init];
    toolbar_settings.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [toolbar_settings setItems:items animated:NO];
    [settingsView addSubview:toolbar_settings];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonSystemItemSave target:self action:@selector(cancelSettings)];
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonSystemItemEdit target:self action:@selector(saveSettings)];
    toolbar_settings.items = [NSArray arrayWithObjects: cancelButton, flexible, saveButton, nil];
    
    // stepper - word counter  
    lbl_settings_wordCount = [[UILabel alloc] initWithFrame:CGRectMake(30, 67, 200, 30)];
    [lbl_settings_wordCount setBackgroundColor:[UIColor clearColor]];
    [lbl_settings_wordCount setTextColor:[UIColor whiteColor]];
    int stepperInt = (int) stepper.value;
    [lbl_settings_wordCount setText:[NSString stringWithFormat:@"Number of words: %i", stepperInt]];
    [settingsView addSubview:lbl_settings_wordCount];
    
    stepper = [[UIStepper alloc] init];
    stepper.frame = CGRectMake(200, 68, 100, 15);
    [stepper addTarget:self action:@selector(stepperChanged) forControlEvents:UIControlEventValueChanged];
    [stepper setMinimumValue:1.0];
    [stepper setMaximumValue:3.0];
    [stepper setStepValue:1.0];
    [stepper setValue:howManyWords];
    [settingsView addSubview:stepper];
    
    // slider - speed regulation
    slider = [[UISlider alloc] initWithFrame:CGRectMake(60, 120, 200.0, 10.0)];
    [slider addTarget:self action:@selector(sliderChanged) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
    slider.minimumValue = 60;//250.0;
    slider.maximumValue = 1020;//1000.0;
    slider.continuous = YES;
    slider.value = 60;//250.0;
    [settingsView addSubview:slider];
    sliderStepValue = 60;//50.0;
    
    lbl_settings_speed = [[UILabel alloc] initWithFrame:CGRectMake(80, 145, 200, 30)];
    [lbl_settings_speed setBackgroundColor:[UIColor clearColor]];
    [lbl_settings_speed setTextColor:[UIColor whiteColor]];
    int sliderInt = (int) slider.value;
    [lbl_settings_speed setText:[NSString stringWithFormat:@"Speed: %i phrases/min", sliderInt]];
    [settingsView addSubview:lbl_settings_speed];
    
}

-(void)loadBook:(id)sender {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [readingView setFrame:CGRectMake(0, 0, 320, 800)];
        [settingslButton setTitle:@"Back"];
        
    }];
    
    actualView = READING_VIEW;
    
    if ([sender tag] == 1) {
        textView.text = @"Gjøvik University College offers studies within the subject areas health and social work, technology, economics and business administration, information technology and media together with a wide range of further training/studies. Several of these studies are uniqe and are only offered at GUC. There are about 3000 students at GUC. You will find lecture room, library, computer rooms, reading room, cantine, book store and more at campus, it contains everything you need as a student.";
        lbl_bookTitle.text = @"HiG";
         choosenBook = 1;
    }
    else if ([sender tag] == 2) {
        textView.text = @"iOS (previously iPhone OS) is a mobile operating system developed and distributed by Apple Inc. Originally released in 2007 for the iPhone and iPod Touch, it has been extended to support other Apple devices such as the iPad and Apple TV. Unlike Microsoft's Windows Phone (Windows CE) and Google's Android, Apple does not license iOS for installation on non-Apple hardware. As of September 12, 2012, Apple's App Store contained more than 700,000 iOS applications, which have collectively been downloaded more than 30 billion times.[3] It had a 14.9% share of the smartphone mobile operating system units shipped in the third quarter of 2012, behind only Google's Android.[4] In June 2012, it accounted for 65% of mobile web data consumption (including use on both the iPod Touch and the iPad).[5] At the half of 2012, there were 410 million devices activated.[6] According to the special media event held by Apple on September 12, 2012, 400 million devices have been sold through June 2012.";
        lbl_bookTitle.text = @"iOS";
        choosenBook = 2;
    }
    else if ([sender tag] == 3) {
        textView.text = @"Norway, officially the Kingdom of Norway, is a Scandinavian unitary constitutional monarchy whose territory comprises the western portion of the Scandinavian Peninsula, Jan Mayen, the Arctic archipelago of Svalbard and the subantarctic Bouvet Island.[note 1] Norway has a total area of 385,252 square kilometres (148,747 sq mi) and a population of about 5 million.[9] It is the second least densely populated country in Europe. The majority of the country shares a border to the east with Sweden; its northernmost region is bordered by Finland to the south and Russia to the east; in its south Norway borders the Skagerrak Strait across from Denmark. The capital city of Norway is Oslo. Norway's extensive coastline, facing the North Atlantic Ocean and the Barents Sea, is home to its famous fjords.";
        lbl_bookTitle.text = @"Norway";
        choosenBook = 3;
    }
    else if ([sender tag] == 4) {
        textView.text = @"Laura is a female given name in Latin Europe whose meaning ('Bay Laurel') translates to everyman, and an early hypocorism from Laurel and Lauren. The name Laura is derived from the bay laurel plant, which, in the Greco-Roman era was used as a symbol of victory, honor or fame. In British North America, it is very likely that the name Laura was extremely popular for female newborns until its rapid decline starting in the late 19th century. The name Laura was among the top 40 names for female newborns for much of the late 19th century in the United States until it dropped off the chart in 1899 at #43. The overall highest known rank for the name Laura in the 19th century was #17 in the years 1880 and 1882 with the male name Samuel. Running up, the name Laura in the 19th century was #19 in 1881 and 1883 with the male name Louis. The name Laura was among the top 50 names for female newborns for much of the early 20th century in the United States, but dropped to the top 100-120 by the 1930s-40s, then rebounded to the top 20 in 1984-1986, and has since steadily decreased in popularity. The overall highest known rank for the name Laura in the 20th century is #10 in 1969 with the male name Richard. The latest highest known rank for the name Laura in the 20th century is #14 in 1985 with the male name Jason.";
        lbl_bookTitle.text = @"Laura";
        choosenBook = 4;
    }
    else if ([sender tag] == 5) {
        textView.text = @"Android is a Linux-based operating system designed primarily for touchscreen mobile devices such as smartphones and tablet computers. Initially developed by Android, Inc., whom Google financially backed and later purchased in 2005,[9] Android was unveiled in 2007 along with the founding of the Open Handset Alliance: a consortium of hardware, software, and telecommunication companies devoted to advancing open standards for mobile devices.[10] The first Android-powered phone was sold in October 2008. Android is open source and Google releases the code under the Apache License.[12] This open source code and permissive licensing allows the software to be freely modified and distributed by device manufacturers, wireless carriers and enthusiast developers. Additionally, Android has a large community of developers writing applications ('apps') that extend the functionality of devices, written primarily in a customized version of the Java programming language.[13] In October 2012, there were approximately 700,000 apps available for Android, and the estimated number of applications downloaded from Google Play, Android's primary app store, was 25 billion.";
        lbl_bookTitle.text = @"Android";
        choosenBook = 5;
    }
    
    lbl_words.text = [NSString stringWithFormat:@"%i", howManyWords];
    lbl_speed.text = [NSString stringWithFormat:@"%i ppm", speed];
    
}



-(void)sliderChanged {

    // size of steps
    float newStep = roundf((slider.value) / sliderStepValue);
    slider.value = newStep * sliderStepValue;
    
    int sliderInt = (int) slider.value;
    [lbl_settings_speed setText:[NSString stringWithFormat:@"Speed: %i phrases/min", sliderInt]];
}

-(void)stepperChanged {
   
    int stepperInt = (int) stepper.value;
    [lbl_settings_wordCount setText:[NSString stringWithFormat:@"Number of words: %i", stepperInt]];
}

// update Main Reading Label
- (void) updateLabel:(id)sender {
        
    NSString *sentence = [NSString stringWithString:textView.text];
    NSMutableArray *arr = [[sentence componentsSeparatedByString: @" "] mutableCopy];   

    lbl.text = @"";
    for (int i = 0; i < howManyWords; i++) {
        if ((count+i) > [arr count]-1) {
            [self resetReading];
            return;
        }
        lbl.text = [lbl.text stringByAppendingString:[arr objectAtIndex:count+i]];
        if (i < howManyWords-1) { // space between words
            lbl.text = [lbl.text stringByAppendingString:@" "];
        }
    }
        
    count += howManyWords;

}

// cancel Settings view
- (void)cancelSettings {
    
    [settingsView setUserInteractionEnabled:NO];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [settingsView setFrame:CGRectMake(0, self.view.frame.size.height, 320, 200)];
        [mainView setAlpha:1.0];
        
    }];
    
    [mainView setUserInteractionEnabled:YES];
        
}

// save Settings view
- (void)saveSettings {
    
    howManyWords = (int) stepper.value;
    speed = slider.value;
    
    [settingsView setUserInteractionEnabled:NO];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [settingsView setFrame:CGRectMake(0, self.view.frame.size.height, 320, 200)];
        [mainView setAlpha:1.0];
        
    }];
    
    [mainView setUserInteractionEnabled:YES];
    
}

// button action to start/stop reading
-(void)btn_startReading {
    
    if (isReading == NO) {
        float speed_converted = (float)60/speed;
        NSLog(@"SPEED: %f", speed_converted);
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:speed_converted target:self selector:@selector(updateLabel:) userInfo:nil repeats:YES];
        self.repeatingTimer = timer;
        [btn_start setTitle:@"Stop reading" forState:UIControlStateNormal];
        UIImage *btn_read = [UIImage imageNamed:@"btn_red.png"];
        [btn_start setBackgroundImage:btn_read
                             forState:UIControlStateNormal];
        isReading = YES;
    }
    else {
        [self resetReading];
    }
}

// button action to show Settings
- (void)btn_showSettings {
    
    if (actualView == MAIN_VIEW) {
    
        [stepper setValue:howManyWords];
        [lbl_settings_wordCount setText:[NSString stringWithFormat:@"Number of words: %i", howManyWords]];

        NSLog(@"SP: %i", speed);
        [slider setValue:speed];
        [lbl_settings_speed setText:[NSString stringWithFormat:@"Speed: %i phrases/min", speed]];
        
        [mainView setUserInteractionEnabled:NO];
        [settingsView setUserInteractionEnabled:YES];
        [settingsView setAlpha:1.0];
        [self.view bringSubviewToFront:settingsView];
        
        [UIView animateWithDuration:0.5 animations:^{
            [settingsView setFrame:CGRectMake(0, self.view.frame.size.height-200, 320, 200)];
            [mainView setAlpha:0.1];
        }];

    }
    else if (actualView == READING_VIEW) {
    
        [UIView animateWithDuration:0.5 animations:^{
            
            [readingView setFrame:CGRectMake(self.view.frame.size.width, 0, 320, 800)];
            [settingslButton setTitle:@"Settings"];
            
        }];
        
        choosenBook = NO; // not needed maybe
        if (isReading == YES) {
            [self resetReading];
        }
        actualView = MAIN_VIEW;
        
    }
}


// reset reading to default state (= move cursor to begin and stop reading)
-(void)resetReading {
    [repeatingTimer invalidate];
    self.repeatingTimer = nil; // stops timer
    [btn_start setTitle:@"Start reading" forState:UIControlStateNormal];
    
    UIImage *btn_read = [UIImage imageNamed:@"btn_green.png"];
    [btn_start setBackgroundImage:btn_read
                         forState:UIControlStateNormal];
    [lbl setText:@""];
    isReading = NO;
    count = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
