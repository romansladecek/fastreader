//
//  fastReaderAppDelegate.h
//  FastReader
//
//  Created by Roman Sladecek on 11/20/12.
//  Copyright (c) 2012 Roman Sladecek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class fastReaderViewController;

@interface fastReaderAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) fastReaderViewController *viewController;

@end
